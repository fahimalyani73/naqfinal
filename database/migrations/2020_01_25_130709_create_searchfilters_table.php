<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSearchfiltersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('searchfilters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('professional_location')->nullable();
            $table->string('project_type')->nullable();
            $table->string('construction_type')->nullable();
            $table->string('plot_location')->nullable();
            $table->string('plot_size')->nullable();
            $table->string('covered_area')->nullable();
            $table->string('profession')->nullable();

            $table->timestamps();
         
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('searchfilters');
    }
}
