<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArtitecturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artitectures', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('company_id');
            $table->string('plot_location');
            $table->string('plot_size');
            $table->string('plot_type');
            $table->string('rate');
            $table->string('details');
            $table->string('get_quotation');
            $table->string('professional_location')->nullable();
            $table->string('no_floor')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('artitectures');
    }
}
