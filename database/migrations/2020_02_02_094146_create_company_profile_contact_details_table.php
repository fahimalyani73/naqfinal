<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyProfileContactDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_profile_contact_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('company_profile_id');
            $table->string('c_created_at');
            $table->text('c_fullname');
            $table->text('c_address');
            $table->text('c_owner_name');
            $table->text('c_mobile_number');
            $table->string('c_email')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_profile_contact_details');
    }
}
