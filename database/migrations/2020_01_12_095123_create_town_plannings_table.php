<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTownPlanningsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('town_plannings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('company_id');
            $table->string('town_location');
            $table->string('covered_area');
            $table->string('rate_sqft');
            $table->string('total');
            $table->string('details');
            $table->string('get_quotation');
            $table->string('professional_location')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('town_plannings');
    }
}
