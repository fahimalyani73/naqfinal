<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIntereiorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('intereiors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('company_id');
            $table->string('plot_location');
            $table->string('covered_area');
            $table->string('rate_sqft');
            $table->string('total');
            $table->string('details');
            $table->string('plot_type');
            $table->string('get_quotation');
            $table->string('professional_location')->nullable();
        
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('intereiors');
    }
}
