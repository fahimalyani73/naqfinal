<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Models\Company;
class companyprofile extends Model
{
    protected $table = 'companyprofiles';
    protected $primaryKey = 'id';

    protected $fillable = ['id','c_id','cp_slider_image','cp_about_us','c_profile_image'];

    function company()
    {
    	return $this->belongsTo('App\Models\Company','c_id');
    }
}
