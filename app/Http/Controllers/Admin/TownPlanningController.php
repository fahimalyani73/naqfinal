<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TownPlanning;
use App\Models\Company;

class TownPlanningController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['cc']= TownPlanning::with('company')->get();
        return view('Admin.TownPlaning.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $company = Company::all();
      return view('Admin.TownPlaning.add_townplanning',compact('company')); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $townp = new TownPlanning();
        $townp->company_id = $request->company_id;
        $townp->town_location = $request->town_location;
        $townp->covered_area = $request->covered_area;
        $townp->rate_sqft = $request->rate_sqft;
        $townp->total = $request->total;
        $townp->details = $request->details;
        $townp->get_quotation = $request->get_quotation;
        $townp->professional_location = $request->professional_location;
        
        $townp->save();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
          $company = Company::all();
          $arch = TownPlanning::find($id);
         
        return view('Admin.TownPlaning.add_townplanning',compact('arch','company'));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $townp = TownPlanning::find($id);
        $townp->company_id = $request->company_id;
        $townp->town_location = $request->town_location;
        $townp->covered_area = $request->covered_area;
        $townp->rate_sqft = $request->rate_sqft;
        $townp->total = $request->total;
        $townp->details = $request->details;
        $townp->get_quotation = $request->get_quotation;
        $townp->professional_location = $request->professional_location;
    
        $townp->save();
        return back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $arch = TownPlanning::find($id)->delete();
        return back()->with('message','Record deleted successfully ');
        //
    }
}
