<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Artitecture;
use App\Models\Company;

class ArchitectureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        $cc=Artitecture::with('company')->get();
          
        return view('Admin.Architecture.index',compact('cc'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $company = Company::all();
        return view('Admin.Architecture.add_architecture',compact('company'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all);
        $townp = new Artitecture();
        $townp->company_id = $request->company_id;
        $townp->plot_location = $request->plot_location;
        $townp->rate = $request->rate;
        $townp->details = $request->details;
        $townp->plot_size = $request->plot_size;
        $townp->plot_type = $request->plot_type;
        $townp->get_quotation = $request->get_quotation;
        $townp->professional_location = $request->professional_location;
        $townp->no_floor = $request->no_floor;
        
        $townp->save();
        return back();  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::all();
        $arch = Artitecture::find($id);
        return view('Admin.Architecture.add_architecture',compact('arch','company'));
        // dd($arch);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $townp = Artitecture::find($id);
        $townp->company_id = $request->company_id;
        $townp->plot_location = $request->plot_location;
        $townp->plot_size = $request->plot_size;
        $townp->rate = $request->rate;
        $townp->details = $request->details;
        $townp->plot_type =$request->plot_type;
        $townp->get_quotation = $request->get_quotation;
        $townp->professional_location = $request->professional_location;
        $townp->no_floor = $request->no_floor;
        
        $townp->save();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $arch = Artitecture::find($id)->delete();
        return back()->with('message','Record deleted successfully ');
    }
}
