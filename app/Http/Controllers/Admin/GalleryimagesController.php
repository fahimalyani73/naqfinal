<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\gallery_images;
use App\Models\gallery_sub_categiry;

class GalleryimagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gimages=gallery_images::with('galllerySubCategory')->get();
       
        return view('Admin.gallaryimages.index', compact('gimages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $gsc = gallery_sub_categiry::all();
        return view('Admin.gallaryimages.gallaryadd',compact('gsc'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $settings = new gallery_images();
        $settings->name = $request->name;
        $settings->gsc_id = $request->gsc_id;
        
        if ($request->file('image_name')) {
            $file = $request->image_name;
            $extension = $file->getClientOriginalExtension();
            $filename = rand(11111, 99999) . '.' . $extension;
            $settings->image_name = $filename;
            $path = public_path('images/settings');
            $file->move($path, $filename);
        }
        $settings->save();
        return redirect()->route('gimages.index');
        return back()->with('message','Data save successfully ');
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $gimages = gallery_images::find($id);
        $gsc = gallery_sub_categiry::all();
        return view('Admin.gallaryimages.gallaryadd',compact('gsc','gimages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $settings = gallery_images::find($id);
        $settings->name = $request->name;
        $settings->gsc_id = $request->gsc_id;
        
        if ($request->file('image_name')) {
            $file = $request->image_name;
            $extension = $file->getClientOriginalExtension();
            $filename = rand(11111, 99999) . '.' . $extension;
            $settings->image_name = $filename;
            $path = public_path('images/settings');
            $file->move($path, $filename);
        }
        $settings->save();
        return redirect()->route('gimages.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gc = gallery_images::find($id)->delete();
        return back()->with('message','Record deleted successfully ');
    }
}
