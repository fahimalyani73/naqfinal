<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CompanyProfileGallery;
use Illuminate\Support\Facades\Input;
use App\companyprofile;

class CompanyProfileGalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $company_profile_gallery=CompanyProfileGallery::with('company')->paginate(20);
        return view('Admin.company.company-profile.profile-gallery.index', compact('company_profile_gallery'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companyprofile = companyprofile::all();
        return view('Admin.company.company-profile.profile-gallery.create',compact('companyprofile'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cpg = new CompanyProfileGallery();
        $cpg->company_profile_id = $request->company_profile_id;
        $cpg->cp_gallery_image_name = $request->cp_gallery_image_name;

        if ($request->file('cp_gallery_image')) {
            $file = $request->cp_gallery_image;
            $extension = $file->getClientOriginalExtension();
            $filename = rand(11111, 99999) . '.' . $extension;
            $cpg->cp_gallery_image = $filename;
            $path = public_path('images/cp_gallery_images');
            $file->move($path, $filename);
        }

        $cpg->save();

        return redirect()->route('company-profile-gallery.index');
        return back();  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $companyprofile = companyprofile::all();
        $company_profile_gallery = CompanyProfileGallery::find($id);
        return view('Admin.company.company-profile.profile-gallery.create',compact('company_profile_gallery','companyprofile'));
        dd($arch);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cpg = CompanyProfileGallery::find($id);
        $cpg->company_profile_id = $request->company_profile_id;
        $cpg->cp_gallery_image_name = $request->cp_gallery_image_name;

        if ($request->file('cp_gallery_image')) {
            $file = $request->cp_gallery_image;
            $extension = $file->getClientOriginalExtension();
            $filename = rand(11111, 99999) . '.' . $extension;
            $cpg->cp_gallery_image = $filename;
            $path = public_path('images/cp_gallery_images');
            $file->move($path, $filename);
        }
        
        $cpg->save();

        return redirect()->route('company-profile-gallery.index');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $arch = CompanyProfileGallery::find($id)>detele();
        return back()>with('message','Record deleted successfully ');
    }
}
