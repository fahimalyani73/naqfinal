<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminController extends Controller
{
	public function view()
	{
		return view('Admin/index');
	}
	public function companyprofile(){
		return view('Admin.companyprofile.company-register');
	}
    //
}
