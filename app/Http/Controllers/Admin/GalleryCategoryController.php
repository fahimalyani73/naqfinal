<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\gallery_category;

class GalleryCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gc = gallery_category::all();
        return view('Admin.SubCategory.index', compact('gc'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Admin.SubCategory.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $settings = new gallery_category();
        $settings->gc_name = $request->gc_name;
        
        if ($request->file('gc_image')) {
            $file = $request->gc_image;
            $extension = $file->getClientOriginalExtension();
            $filename = rand(11111, 99999) . '.' . $extension;
            $settings->gc_image = $filename;
            $path = public_path('images/settings');
            $file->move($path, $filename);
        }
        
        $settings->save();
        return redirect()->route('gallery.index');
        return back()->with('message','Data save successfully ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $gc = gallery_category::find($id);
        return view('Admin.SubCategory.add',compact('gc'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $gc = gallery_category::find($id);
        $gc->gc_name = $request->gc_name;
        
        if ($request->file('gc_image')) {
            $file = $request->gc_image;
            $extension = $file->getClientOriginalExtension();
            $filename = rand(11111, 99999) . '.' . $extension;
            $gc->gc_image = $filename;
            $path = public_path('images/settings');
            $file->move($path, $filename);
        }
        
        $gc->save();
        return redirect()->route('gallery.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gc = gallery_category::find($id)->delete();
        return back()->with('message','Record deleted successfully ');
    }
}
