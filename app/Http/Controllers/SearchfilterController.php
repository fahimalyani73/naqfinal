<?php

namespace App\Http\Controllers;

use App\Models\searchfilter;
use Illuminate\Http\Request;

class SearchfilterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\searchfilter  $searchfilter
     * @return \Illuminate\Http\Response
     */
    public function show(searchfilter $searchfilter)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\searchfilter  $searchfilter
     * @return \Illuminate\Http\Response
     */
    public function edit(searchfilter $searchfilter)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\searchfilter  $searchfilter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, searchfilter $searchfilter)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\searchfilter  $searchfilter
     * @return \Illuminate\Http\Response
     */
    public function destroy(searchfilter $searchfilter)
    {
        //
    }
}
