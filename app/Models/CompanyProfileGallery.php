<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Company;
class CompanyProfileGallery extends Model
{
    protected $table = 'company_profile_galleries';
    protected $primaryKey = 'id';

    protected $fillable = ['id','cp_id','cp_gallery_image_name','cp_gallery_image'];
    function company()
    {
    	return $this->belongsTo('App\Models\Company','company_profile_id');
    }
}
