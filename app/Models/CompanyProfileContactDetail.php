<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Company;
class CompanyProfileContactDetail extends Model
{
    protected $table = 'company_profile_contact_details';
    protected $primaryKey = 'id';

    protected $fillable = ['id','cp_id','c_created_at','c_fullname','c_address','c_owner_name','c_mobile_number','c_email'];

     function company()
    {
    	return $this->belongsTo('App\Models\Company','company_profile_id');
    }
}
