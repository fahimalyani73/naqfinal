<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'companies';
    protected $primaryKey = 'id';

    protected $fillable = ['id','c_name','c_type','c_drawing_detail','c_profile_image'];

    function companyProfile()
    {
    	return $this->belongsTo('App\Models\Company','company_id');
    }

    public function companyProfileContactDetail(){
    	return $this->belongsTo('App\Models\CompanyProfileContactDetail','company_profile_id');
    }
}
