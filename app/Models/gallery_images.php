<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class gallery_images extends Model
{
	protected $table ="gallery_images";

	protected $primarykey ="id";
    
    public function galllerySubCategory(){
		return $this->belongsTo(gallery_sub_categiry::class,'gsc_id');
	}
}
