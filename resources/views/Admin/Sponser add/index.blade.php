@extends('Admin.admin-layout')

@push('css')
@endpush
@section('content')
<div class="container-fluid">
   <!-- BEGIN PAGE HEADER-->   
   <div class="row-fluid">
      <div class="span12">
         <!-- BEGIN THEME CUSTOMIZER-->
      
         <!-- END THEME CUSTOMIZER-->
         <!-- BEGIN PAGE TITLE & BREADCRUMB-->
         <h3 class="page-title">
            Architecture
         </h3>
        
         <!-- END PAGE TITLE & BREADCRUMB-->
      </div>
   </div>
   <!-- END PAGE HEADER-->
   <!-- BEGIN EDITABLE TABLE widget-->
   <div class="row-fluid">
      <div class="span12">
         <!-- BEGIN EXAMPLE TABLE widget-->
         <div class="widget purple">
            <div class="widget-title">
               <h4><i class="icon-reorder"></i> Architecture</h4>
               <span class="tools">
               <a href="javascript:;" class="icon-chevron-down"></a>
               <a href="javascript:;" class="icon-remove"></a>
               </span>
            </div>
            <div class="widget-body">
               <div>
                  <div class="clearfix">
                     <div class="btn-group">
                        <a href="{{ route('sponseradd.create') }}" id="editable-sample_new" class="btn green">
                        Add New <i class="icon-plus"></i>
                        </a>
                     </div>
                 
                  </div>
                  <div class="space15"></div>
                  <div id="editable-sample_wrapper" class="dataTables_wrapper form-inline" role="grid">
                     <div class="row-fluid">
                        <div class="span6">
                           <div id="editable-sample_length" class="dataTables_length">
                              <label>
                                 <select size="1" name="editable-sample_length" aria-controls="editable-sample" class="xsmall">
                                    <option value="5" selected="selected">5</option>
                                    <option value="15">15</option>
                                    <option value="20">20</option>
                                    <option value="-1">All</option>
                                 </select>
                                 records per page
                              </label>
                           </div>
                        <div class="span6">
                        </div>
                           <div class="dataTables_filter" id="editable-sample_filter"><label>Search: <input type="text" aria-controls="editable-sample" class="medium"></label></div>
                        </div>
                     </div>
                     <table class="table table-striped table-hover table-bordered dataTable" id="editable-sample" aria-describedby="editable-sample_info">
                        <thead>

                           <tr role="row">
                              <th>#</th>
                              <th class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="Username" style="width: 193px;">name</th>
                              <th class="sorting" role="columnheader" tabindex="0" aria-controls="editable-sample" rowspan="1" colspan="1" aria-label="Full Name: activate to sort column ascending" style="width: 288px;">gsc_id</th>
                              <th class="sorting" role="columnheader" tabindex="0" aria-controls="editable-sample" rowspan="1" colspan="1" aria-label="Full Name: activate to sort column ascending" style="width: 288px;">image_name</th>
                              
                              <th class="sorting" role="columnheader" tabindex="0" aria-controls="editable-sample" rowspan="1" colspan="1" aria-label="Edit: activate to sort column ascending" style="width: 89px;">Edit</th>
                              <th class="sorting" role="columnheader" tabindex="0" aria-controls="editable-sample" rowspan="1" colspan="1" aria-label="Delete: activate to sort column ascending" style="width: 129px;">Delete</th>
                           </tr>
                        </thead>
                        <tbody role="alert" aria-live="polite" aria-relevant="all">
                           @foreach($gc as $index => $value)
                        
                           <tr class="odd">
                              <td>{{ ++$index }}</td>
                              <td class="  sorting_1">{{ $value->name }}</td>
                              <td class="  sorting_1">{{ $value->image_name }}</td>
                              <td class="  sorting_1">{{ $value->gsc_id }}</td>
                              <td class=" ">
                                 <img width="100px" height="100px" style="border-radius: 100%;" src="{{ asset('images/settings'.'/'.$value->gc_image) }}">
                              </td>
                              
                              <td class=" "><a class="edit" href="{{ route('Architecture.edit',$value->id) }}">Edit</a></td>
                              <td class=" ">
                                 <a class="delete" href="javascript::void();" onclick="event.preventDefault();
                                                     document.getElementById('delete-arch-{{ $value->id }}').submit();">
                                                  Delete
                                 </a>
                                <form id="delete-arch-{{ $value->id }}" action="{{ route('Architecture.destroy',$value->id) }}" method="POST" style="display: none;">
                                 @method('delete')
                                        @csrf
                                </form>
                              </td>
                           </tr>
                           @endforeach
                        
                        </tbody>
                     </table>
                     <div class="row-fluid">
                        <div class="span6">
                           <div class="dataTables_info" id="editable-sample_info">Showing 1 to 5 of 12 entries</div>
                        </div>
                        <div class="span6">
                           <div class="dataTables_paginate paging_bootstrap pagination">
                              <ul>
                                 <li class="prev disabled"><a href="#">← Prev</a></li>
                                 <li class="active"><a href="#">1</a></li>
                                 <li><a href="#">2</a></li>
                                 <li><a href="#">3</a></li>
                                 <li class="next"><a href="#">Next → </a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- END EXAMPLE TABLE widget-->
      </div>
   </div>
   <!-- END EDITABLE TABLE widget-->
</div>
@endsection

@push('js')

@endpush