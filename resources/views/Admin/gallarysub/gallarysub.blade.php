@extends('Admin.admin-layout') @push('css') @endpush @section('content')

<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->

    <!-- BEGIN PAGE CONTENT-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="widget red">
                <div class="widget-title">
                    <h4><i class=" icon-key"></i>Construction</h4>
                    <span class="tools">
               <a href="javascript:;" class="icon-chevron-down"></a>
               <a href="javascript:;" class="icon-remove"></a>
               </span>
                </div>
                <div class="widget-body form">
                    <!-- BEGIN FORM-->
                    @if(isset($gsc) && !empty($gsc))
                    <form class="form-horizontal" method="post" action="{{ route('gallarysub.update',$gsc->id) }} " enctype="multipart/form-data">
                        @method('put') @else
                        <form class="form-horizontal" method="post" action="{{ route('gallarysub.store') }}" enctype="multipart/form-data">
                            @endif @csrf
                            
                            <div class="control-group success">
                                <label class="control-label" for="gc_id">Gallery Category </label>
                                <div class="controls">
                                    <select class="span6" name="gc_id" id="gc_id" required="">
                                        <option value="">Please Select Gallery Category</option>
                                        @foreach($gc as $value)
                                        <option value="{{ $value->id }}" @if(isset($gsc) && $value->id = $gsc->gc_id) selected="selected" @endif>{{ $value->gc_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>


                                <div class="control-group error col-sm-12">
                                    <label class="control-label" for="inputError">Gallery Subcategory name</label>
                                    <div class="controls">
                                        <input type="text" name="name" id="name" class="form-control" @if(isset($gsc) && !empty($gsc->name)) value="{{ $gsc->name }}" @endif>
                                    </div>
                                </div>
                                <div class="control-group error col-sm-12">
                                    <label class="control-label" for="inputError">Image</label>
                                    <div class="controls">
                                        <input type="file" name="image_name" id="image_name" class="form-control">
                                    </div>
                                </div>
                               
                               

                                <div class="form-actions">
                                    <button type="submit" class="btn btn-success">
                                        @if(isset($arch) && !empty($arch)) Update @else Save @endif
                                    </button>
                                    <button type="button" class="btn">Cancel</button>
                                </div>
                        </form>
                        <!-- END FORM-->
                        </div>
                </div>
                <!-- END VALIDATION STATES-->
            </div>
        </div>

@endsection 
@push('js') 

@endpush


