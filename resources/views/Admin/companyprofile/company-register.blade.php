

<!DOCTYPE html>
<html lang="en-US" class="no-js no-svg">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-143451018-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-143451018-1');
</script>



    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window,document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
     fbq('init', '1263632953811061'); 
    fbq('track', 'PageView');
    </script>
    <noscript>
     <img height="1" width="1" 
    src="https://www.facebook.com/tr?id=1263632953811061&ev=PageView
    &noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->

    <title>Mera Ghar Mera Naqsha | Connecting Public and Professionals</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=0">
    <meta charset="UTF-8">
    <meta name="theme-color" content="#101214"  /><!--content="#212529"-->
    <meta property="og:url" 	           content="https://www.naqsha.com.pk" />
    <meta property="og:type"               content="Website" />
    <meta property="og:title"              content="Pakistan's first online platform to bridge the gap between Professionals and Public" />
    <meta property="og:description"        content="Naqsha.com.pk helps public to find the best Architects, Constructors, Interior and Landscape Designers all over Pakistan." />
    <meta property="og:image" 	           content="https://www.naqsha.com.pk/wp-content/uploads/2019/07/og-img.jpg" />
    <meta property="fb:app_id" 	           content="270023598195" />
    <meta name="twitter:card"	  content="summary" />
    <meta name="twitter:site"	  content="@naqshaofficial"  />
    <meta name="twitter:title" 	  content="Pakistan's first online platform to bridge the gap between Professionals and Public" />
    <meta name="twitter:description"  content="Naqsha.com.pk helps public to find the best Architects, Constructors, Interior and Landscape Designers all over Pakistan." />
    <meta name="twitter:image"	  content="https://www.naqsha.com.pk/wp-content/uploads/2019/07/og-img.jpg" />
    <meta name="twitter:image:alt"	  content="www.naqsha.com.pk" />
    <meta name="description" content="Naqsha is Pakistan's first online platform to bridge the gap between Professionals and Public - Naqsha helps public to find the best Architects, Constructors, Interior and Landscape Designers all over Pakistan." />
    
    <!--<link rel='manifest' href='/wp-content/themes/naqsha/manifest.json'>-->
    <link rel="profile" href="http://gmpg.org/xfn/11">
    
    <!-- <script src="/wp-content/themes/naqsha/pwa-sw-register.js" async defer></script> -->
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <!-- External CSS libraries -->
    <link rel="stylesheet" type="text/css" href="https://www.naqsha.com.pk/wp-content/themes/naqsha/css/styles.css">
     <link rel="stylesheet" href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" />
    <!-- Favicon icon -->
    <link rel="shortcut icon" href="https://www.naqsha.com.pk/wp-content/themes/naqsha/img/favicon-1.png" type="image/x-icon" >

    
    
<!-- This site is optimized with the Yoast SEO Premium plugin v12.1 - https://yoast.com/wordpress/plugins/seo/ -->
<link rel="canonical" href="https://www.naqsha.com.pk/registered-companies/" />
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="article" />
<meta property="og:title" content="Register Company - Mera Ghar Mera Naqsha | Connecting Public and Professionals" />
<meta property="og:url" content="https://www.naqsha.com.pk/registered-companies/" />
<meta property="og:site_name" content="Mera Ghar Mera Naqsha | Connecting Public and Professionals" />
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:title" content="Register Company - Mera Ghar Mera Naqsha | Connecting Public and Professionals" />
<script type='application/ld+json' class='yoast-schema-graph yoast-schema-graph--main'>{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"https://www.naqsha.com.pk/#website","url":"https://www.naqsha.com.pk/","name":"Mera Ghar Mera Naqsha | Connecting Public and Professionals","potentialAction":{"@type":"SearchAction","target":"https://www.naqsha.com.pk/?s={search_term_string}","query-input":"required name=search_term_string"}},{"@type":"WebPage","@id":"https://www.naqsha.com.pk/registered-companies/#webpage","url":"https://www.naqsha.com.pk/registered-companies/","inLanguage":"en-US","name":"Register Company - Mera Ghar Mera Naqsha | Connecting Public and Professionals","isPartOf":{"@id":"https://www.naqsha.com.pk/#website"},"datePublished":"2019-12-09T19:40:01+00:00","dateModified":"2019-12-09T19:43:18+00:00"}]}</script>
<!-- / Yoast SEO Premium plugin. -->

<link rel='shortlink' href='https://www.naqsha.com.pk/?p=7054' />
		<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
		</head>

<body >
		
<!--<div class="page_loader"></div>-->
<div class="container-fulid" style="background-color:black;">
	<div class="row" style="padding: 3px;
    margin: 0px !important;
    color: white;
    font-size: 12px;">
	
	<div class="col-md-6 d-none d-sm-none d-md-block  text-left" style="color:white !important" >
						<a href="https://www.facebook.com/naqshaofficialpk"  style="color:white !important;padding:0px 5px;" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a>
						<a href="https://twitter.com/naqshaofficial"  style="color:white !important;padding:0px 5px;" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a>
						<a href="https://www.instagram.com/naqshaofficial/"  style="color:white !important;padding:0px 5px;" target="_blank" class="google"><i class="fa fa-instagram"></i></a>
						 
							<a href="https://www.linkedin.com/company/naqshaofficial/"  style="color:white !important;padding:0px 5px;" target="_blank" class="linkedin"><i class="fa fa-linkedin"></i></a>
                
                    </div>
	<div class="col-xs-12 col-sm-12 col-md-6 text-right" style="color:white !important" >You Want To register? Call us <a href="https://api.whatsapp.com/send?phone=923114263343&text=I+want+to+get+registered+with+naqsha.com.pk" style="color:white !important">+9231-142-63343</a></div>
	</div>
</div>
<header class="main-header header-transparent sticky-header" id="head">

     <div class="container">
         <nav class="navbar navbar-expand-lg navbar-dark">
             <a class="navbar-brand logo" href="https://www.naqsha.com.pk">
                <img src="{{ asset('public/assets/images/naqshalogo.jpeg') }}" alt="logo">
            </a>
           <!--<img class="d-none"  src="https://www.naqsha.com.pk/wp-content/themes/naqsha/img/naqsha-slogan.png" style="margin-top: 3%;/* top: 85%; *//* float: left; *//* margin-right: 34px; */width: 280px;height: 50px;/* margin-left: 50% !important; */" alt="logo">          -->
           <button class="navbar-toggler" id="sidebarCollapse" type="button"  aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
			<div id="get_quotation" class="modal modal-xl" role="dialog">
  <div class="modal-dialog" style="max-width:630px;" >

    <!-- Modal content-->
    <div class="modal-content">
     
      <div class="modal-body" style="    text-align: right;   padding:0px">
	  <i class="fa fa-window-close fa-2x" aria-hidden="true" style=' position: absolute;
   
    top: -5px;
    right: -1px;'  data-dismiss="modal"></i>
       <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/12/new-info.webp?v-1.2" width="100%">
      </div>
     
    </div>

  </div>
</div>
           
           
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul class="navbar-nav header-ml ml-auto ">
					 <li class="nav-item dropdown">
                        <a class="nav-link"  href="#" data-toggle="modal" data-target="#get_quotation"  aria-haspopup="true" aria-expanded="false">
                            How to Get Quotation
                        </a>
                       
                    </li>
					
                    <li class="nav-item dropdown active">
                        <a class="nav-link dropdown-toggle" href="https://www.naqsha.com.pk/architecture" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Services 
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <li><a class="dropdown-item" href="https://www.naqsha.com.pk/architecture">Architecture</a></li>
                            <li><a class="dropdown-item" href="https://www.naqsha.com.pk/construction">Construction</a></li>
                            <li><a class="dropdown-item" href="https://www.naqsha.com.pk/interior">Interior</a></li>
                            <li><a class="dropdown-item" href="https://www.naqsha.com.pk/landscape">Landscape</a></li>
                           
                           <li><a class="dropdown-item" href="https://www.naqsha.com.pk/town-planner">Town Planning</a></li>
                           <li><a class="dropdown-item" href="https://www.naqsha.com.pk/engineers">Engineering</a></li>
                        </ul>
                    </li>
							
									
					<li class="nav-item dropdown">
                        <a class="nav-link" href="https://www.naqsha.com.pk/about-us">
                            About us
                        </a>
                    </li>   
					<li class="nav-item dropdown">
                        <a class="nav-link" href="https://www.naqsha.com.pk/terms">
                            Terms & Conditions
                        </a>
                    </li>  
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="https://www.naqsha.com.pk/faq">
                            FAQs
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="https://www.naqsha.com.pk/blogs">
                            Blogs    
                        </a>                                           
                    </li>  
                   
                     <li class="nav-item dropdown">
                        <a class="nav-link" id='btn-contact-us' >
                            Contact Us
                        </a>
                       
                    </li>
                 
                </ul>

               

            </div>
        </nav>

    <nav class="sidenav"></nav>
 <!-- Sidebar  -->
        <nav id="sidebar" class=" d-lg-none ">
            <div id="dismiss">
                <i class="fa fa-angle-double-right"></i>
            </div>

            <div class="sidebar-header">
                <h3>&nbsp;</h3>
            </div>

                        <ul class="list-unstyled components">
               <!--  <li class="active">

                </li> -->
				<li>
                    <a href='#' data-toggle="modal" data-target="#get_quotation"> How to Get Quotation</a>
                </li>
                <li>
                    
                    <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false">Services</a>
                    <ul class="collapse list-unstyled" id="pageSubmenu">
                         <li><a  href="https://www.naqsha.com.pk/architecture">Architecture</a></li>
                            <li><a  href="https://www.naqsha.com.pk/construction">Construction</a></li>
                            <li><a  href="https://www.naqsha.com.pk/interior">Interior</a></li>
                            <li><a  href="https://www.naqsha.com.pk/landscape">Landscape</a></li>
                            <li><a  href="https://www.naqsha.com.pk/town-planner">Town Planning</a></li>
                            <li><a  href="https://www.naqsha.com.pk/engineers">Engineering</a></li>
                    </ul>
                </li>
                <li>
					<a class="nav-link" href="https://www.naqsha.com.pk/about-us">
                            About us
                    </a>
				</li>
				<li>
					<a class="nav-link" href="https://www.naqsha.com.pk/terms">
                            Terms & Conditions
                    </a>
				</li>
                <li>
					<a class="nav-link" href="https://www.naqsha.com.pk/faq">
                            FAQs
                    </a>
				</li>
                <li>
					<a class="nav-link" href="https://www.naqsha.com.pk/blogs">
                            Blogs
                    </a>
				</li>
                <li>
                    <a id='btn-contact-us-mobi'>Contact Us</a>
                </li>
							<li class="nav-item dropdown">
                    
                    <a href="#pageSubmenu1" data-toggle="collapse" aria-expanded="false">Registered Companies</a>
                  <!--   <ul class="collapse list-unstyled" id="pageSubmenu1">
						<li><a  href="https://www.naqsha.com.pk/registered-companies/?cat=arch">Architecture <span class="arch_count_id"> </span></a></li>
						<li><a  href="https://www.naqsha.com.pk/registered-companies/?cat=con">Construction <span class="con_count_id"></span></a></li>
						<li><a  href="https://www.naqsha.com.pk/registered-companies/?cat=inter">Interior <span class="inter_count_id"></span></a></li>
						<li><a  href="https://www.naqsha.com.pk/registered-companies/?cat=land">Landscape <span class="land_count_id"></span></a></li>
                    </ul> -->
                </li>		
            </ul>


          <!--   <ul class="list-unstyled CTAs">
                <li>
                    <a href="https://bootstrapious.com/tutorial/files/sidebar.zip" class="download">Download source</a>
                </li>
                <li>
                    <a href="https://bootstrapious.com/p/bootstrap-sidebar" class="article">Back to article</a>
                </li>
            </ul> -->
        </nav>
    </div>
    <div class="overlay"></div>
</header>


<!-- Listing banner start -->
<div class="overview-bgi listing-banner" style="background-image:url('https://www.naqsha.com.pk/wp-content/themes/naqsha/img/cover/arch.jpg ');">
    <div class="container listing-banner-info">
        <div class="row">
            <div class="col-lg-12 col-md-12 clearfix">
                <div class="text">
                    <h1> Registered Companies </h1>
                   
                </div>
            </div>
            <div class="col-lg-5 col-md-12 clearfix">
                <div class="cover-buttons">
                    <ul>
                      
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Listing banner end -->

<!-- Listing section start -->
<div class="listing-section content-area d-print-none"  style="background-image:url('https://www.naqsha.com.pk/wp-content/themes/naqsha/img/bg_cover.png ');" >
	<div class="container" >
		<div class="row">
			<div class="col col-sm-12">
				<table id="example" class="display dt-responsive table table-bordered mb-0 data_table_th" style="width:100%">
					<thead class="bg-active">
						<tr>
						  
							<th>Company Name</th>
							<th>Portfolio</th>
							
						</tr>
					</thead>
			</table>
		   </div>
		</div>
	</div>
</div>
   <style type="text/css">
@media print { 
 #myModal{
   
 -webkit-print-color-adjust: exact;

 background-image:url(https://www.naqsha.com.pk/wp-content/themes/naqsha/img/bg-email.jpg);
 }
 
}
</style> 
 




<!-- Listing section end -->
<div class="d-print-none">
<!-- Footer start -->
<footer class="footer" style="background-image:url('https://www.naqsha.com.pk/wp-content/themes/naqsha/img/footer.png ');  ">
    <div class="container footer-inner">
        <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-12" data-aos="fade-right" data-aos-duration="4000">
				<h4 class="footer-h4">About Us</h4>
				<hr class="footer-hr">
                <div class="footer-item clearfix">
                    <img src="{{ asset('public/assets/images/naqshalogo.jpg') }}" alt="logo" class="f-logo" style="height: 35px !important">
                    <div class="text">
                        <p style="text-align: justify !important; line-height: 17px !important;">Naqsha has revolutionized the construction sector of Pakistan by providing online real-time quotations in just three simple steps.</p>
						
					</div>
                </div>
            </div>
           
    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12" data-aos="fade-up" data-aos-duration="4000">
		 <div class="offset-lg-2 col-lg-8 offset-lg-2 col-sm-12" style="padding-left:0px !important;">
		   <h4 class="footer-h4 h4-center">Quick Links</h4>
				<hr class="footer-hr hr-center"></div>
		  <div class="row">
            
          <div class="offset-xl-2 col-xl-4 col-lg-6 col-md-6 col-sm-6 col-xs-12">

                <div class="footer-item">
                   
                   
                     <ul class="links">
                        <li>
                            <a href="https://www.naqsha.com.pk">&#8250; Home</a>
                        </li>
                        <li>
                            <a href="https://www.naqsha.com.pk/about-us">&#8250; About Us</a>
                        </li>
                         <li>
                            <a href="https://www.naqsha.com.pk/terms">&#8250; Terms & Conditions</a>
                        </li>
                         <li>
                            <a href="https://www.naqsha.com.pk/faq">&#8250; FAQs</a>
                        </li>
                         <li>
                            <a href="https://www.naqsha.com.pk/blogs">&#8250; Blogs</a>
                        </li>
                       
                       
                    </ul>
                </div>
            </div>
            <div class="offset-xl-1 col-xl-5 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="footer-item">
                   
                  
                    <ul class="links">
                        <li>
                            <a href="https://www.naqsha.com.pk/architecture">&#8250; Architecture</a>
                        </li>
                        <li>
                            <a href="https://www.naqsha.com.pk/construction">&#8250; Construction</a>
                        </li>
                         <li>
                            <a href="https://www.naqsha.com.pk/interior">&#8250; Interior</a>
                        </li>
                         <li>
                            <a href="https://www.naqsha.com.pk/landscape">&#8250; Landscape</a>
                        </li>
                         <li>
                            <a href="https://www.naqsha.com.pk/town-planner">&#8250; Town Planning</a>
                        </li>
                         <li>
                            <a href="https://www.naqsha.com.pk/engineers">&#8250; Engineering</a>
                        </li>
                       
                       
                    </ul>
                </div>
            </div>
		  </div>
		  </div>

             <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-12" data-aos="fade-left" data-aos-duration="4000">
        
                 <div class="footer-item" id="contact_us">
                   <h4 class="footer-h4">Contact Us</h4>
				<hr class="footer-hr">
                    <ul class="contact-info">
                        <li style="display:none">
                            <i class="fa fa-map-marker"></i>13 – Tipu Block, New Garden Town, Lahore.
                        </li>
                         <li style="display:none">
                            <i class="fa fa-map-marker"></i>16 – Block B, Defence View Society Phase-1 DHA, Karachi.
                        </li>
                        <li>
                            <i class="fa fa-envelope-o"></i><a href="mailto:info@naqsha.com.pk">info@naqsha.com.pk</a>
                        </li>
                        <li>
                            <i class="fa fa-mobile fa-2x"></i><a href="tel:+92-301-3077-481">+92-301-3077-481 Lahore Office</a>
                        </li>
                        <li>
                            <i class="fa fa-mobile fa-2x" ></i><a href="tel:+92-333-037-4327">+92-333-0374327 Karachi Office</a>
                        </li>
                        <li>
                            <i class="fa fa-phone " ></i><a href="tel:+92-423-5913-810">+92-423-5913-810</a>
                        </li>
                    </ul>
                </div> 
            </div>
			
        </div>
    </div>
</footer>
<!-- Footer end -->
<!-- Sub footer start -->
<div class="sub-footer">
    <div class="container">
        <div class="row ">
             <div class="col-lg-4 col-md-4">
                <p class="copy">© 2020 <a href="https://www.naqsha.com.pk">naqsha.com.pk</a></p>
            </div>
         
            <div class="col-lg-3 col-md-4">
                 <ul class="social-list clearfix text-center" style="font-size: xx-large;">
                    <li><a href="https://www.facebook.com/naqshaofficialpk" target="_blank"  class="facebook"><i style="color:white;" class="fa fa-facebook"></i></a></li>
                    <li><a href="https://twitter.com/naqshaofficial" target="_blank" class="twitter"><i style="color:white;" class="fa fa-twitter"></i></a></li>
                    <li><a href="https://www.instagram.com/naqshaofficial/" target="_blank" class="google"><i style="color:white;" class="fa fa-instagram"></i></a></li>
                    <li><a href="https://www.linkedin.com/company/naqshaofficial/" target="_blank" class="linkedin"><i style="color:white;" class="fa fa-linkedin"></i></a></li>
                </ul>
            </div>
              
        </div>
    </div>
</div>
<!-- Sub footer end -->

<!-- Full Page Search -->
<div id="full-page-search">
    <button type="button" class="close">×</button>
    <form action="https://www.naqsha.com.pk">
        <input type="search" value="" placeholder="type keyword(s) here" />
        <button type="submit" class="btn btn-sm button-theme">Search</button>
    </form>
</div>
<a id="page_scroller" href="#top" style="position: fixed; z-index: 2147483647; display: none; bottom: 100px !important;"><i class="fa fa-chevron-up"></i></a>
<a href="https://api.whatsapp.com/send?phone=923114263343&text=I+want+to+get+registered+with+naqsha.com.pk"  style='    position: fixed;
    bottom: 79px;
    right: 20px;'><img src="https://www.naqsha.com.pk/wp-content/themes/naqsha/img/waicon.png" width='60px'></a>

 
<script>
var logo_url="https://www.naqsha.com.pk";
</script>

<script src="https://www.naqsha.com.pk/wp-content/themes/naqsha/js/jquery-2.2.0.min.js"></script>
<script defer type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.9/jquery.lazy.min.js"></script>
<script defer type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.9/jquery.lazy.plugins.min.js"></script>
<!-- <script defer src="https://www.naqsha.com.pk/wp-content/themes/naqsha/js/popper.min.js"></script> -->
<script defer src="https://www.naqsha.com.pk/wp-content/themes/naqsha/js/bootstrap.min.js"></script>
<!-- <script defer src="https://www.naqsha.com.pk/wp-content/themes/naqsha/js/bootstrap-submenu.min.js"></script> -->
<!--<script defer src="https://www.naqsha.com.pk/wp-content/themes/naqsha/js/rangeslider.js"></script>-->
<!-- <script defer src="https://www.naqsha.com.pk/wp-content/themes/naqsha/js/jquery.mb.YTPlayer.min.js"></script> -->
<!-- <script defer src="https://www.naqsha.com.pk/wp-content/themes/naqsha/js/bootstrap-select.min.js"></script> -->
<!--<script defer src="https://www.naqsha.com.pk/wp-content/themes/naqsha/js/jquery.easing.1.3.js"></script>-->
<script defer src="https://www.naqsha.com.pk/wp-content/themes/naqsha/js/jquery.scrollUp.js"></script>
<!--<script defer src="https://www.naqsha.com.pk/wp-content/themes/naqsha/js/jquery.mCustomScrollbar.concat.min.js"></script>-->
<!-- <script defer src="https://www.naqsha.com.pk/wp-content/themes/naqsha/js/leaflet.min.js"></script> -->
<!-- <script defer src="https://www.naqsha.com.pk/wp-content/themes/naqsha/js/leaflet-providers.min.js"></script> -->
<!-- <script defer src="https://www.naqsha.com.pk/wp-content/themes/naqsha/js/leaflet.markercluster.min.js"></script> -->
<!--<script defer src="https://www.naqsha.com.pk/wp-content/themes/naqsha/js/moment.min.js"></script>-->
<!-- <script defer src="https://www.naqsha.com.pk/wp-content/themes/naqsha/js/daterangepicker.min.js"></script>
<script defer src="https://www.naqsha.com.pk/wp-content/themes/naqsha/js/dropzone.min.js"></script> -->
<script defer src="https://www.naqsha.com.pk/wp-content/themes/naqsha/js/slick.min.js"></script>
<!--<script defer src="https://www.naqsha.com.pk/wp-content/themes/naqsha/js/jquery.filterizr.min.js"></script>-->
<!-- <script defer src="https://www.naqsha.com.pk/wp-content/themes/naqsha/js/jquery.magnific-popup.min.js"></script> -->
<!-- <script defer src="https://www.naqsha.com.pk/wp-content/themes/naqsha/js/jquery.countdown.min.js"></script> -->
<!-- <script defer src="https://www.naqsha.com.pk/wp-content/themes/naqsha/js/maps.min.js"></script> -->
<script defer src="https://www.naqsha.com.pk/wp-content/themes/naqsha/js/app.min.js?v2"></script>
<script defer src="https://www.naqsha.com.pk/wp-content/themes/naqsha/js/datatables.min.js"></script>
<script defer src="https://www.naqsha.com.pk/wp-content/themes/naqsha/js/select2.min.js"></script>

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script defer  src="https://www.naqsha.com.pk/wp-content/themes/naqsha/js/ie10-viewport-bug-workaround.js"></script>
<!-- Custom javascript -->
<script  src="https://www.naqsha.com.pk/wp-content/themes/naqsha/js/ie10-viewport-bug-workaround.js"></script>
  <script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>
  <script>
    AOS.init({
  duration: 1200,
  disable: 'mobile'
});

  </script>
<script>
$(document).ready(function () {
  $('.lazy').lazy({
            delay: 2000,
        });

    // $("#sidebar").mCustomScrollbar({
    //                 theme: "minimal"
    //             });
$('.select2').select2();
                $('#dismiss, .overlay').on('click', function () {
                    $('#sidebar').removeClass('active');
                    $('.overlay').removeClass('active');
                });

                $('#sidebarCollapse').on('click', function () {
                    $('#sidebar').addClass('active');
                    $('.overlay').addClass('active');
                    $('.collapse.in').toggleClass('in');
                    $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });

 
    
    $('#projectType').on('change',function(){ 
        var value = $(this).val();
        
        if(value == 'commercial') {	
            $('#ConstructionType option[value="double_story"]').remove();
            $('#ConstructionType').append('<option value="floors" >No of Floors (1-12)</option>');
        }else if(value == 'residential') {
            $('#ConstructionType option[value="floors"]').remove();
            $('#ConstructionType').append('<option value="double_story" >Double Story</option>');
        }
    });

	var loc = window.location.origin+window.location.pathname;
	if(loc =='https://www.naqsha.com.pk'){
		$("#btn-how-it-work").removeAttr("href");
		$("#btn-how-it-work-mobi").removeAttr("href");
    }

    $(".bi-4 .banner-info ul li").on( "click", function() {

    $(".bi-4 .banner-info ul li a").removeClass("home-cat");
	$(".bi-4 .banner-info ul li a h5").removeAttr("style");

    //$('a',this).css('background-color','#FFD400');
    $('a',this).addClass("home-cat");
	

	$('a.home-cat h5',this).css({color:"black"});

});
    
	setTimeout(function(){
	   if($('#noticepopup').length){
			$('#noticepopup').modal('show');
	   }
   }, 500);  
});

function select_my_city(name){
$('#nameofcity').empty();
    $('#nameofcity').append(name);
$('#SelectCity').modal('hide')
}
function select_my_area(name){
$('#nameofarea').empty();
    $('#nameofarea').append(name);
$('#SelectArea').modal('hide')
}
 $('#btn-how-it-work').click(function() {
                $('html, body').animate({
                    scrollTop: $("#how-it-works").offset().top
                }, 1000);
 });
 $('#btn-contact-us').click(function() {
                $('html, body').animate({
                    scrollTop: $("#contact_us").offset().top
                }, 1000);
 });
 $('#btn-how-it-work-mobi').click(function() {
                $('html, body').animate({
                    scrollTop: $("#how-it-works").offset().top
                }, 1000);
				 $('#sidebar').removeClass('active');
                    $('.overlay').removeClass('active');
 });
 $('#btn-contact-us-mobi').click(function() {
                $('html, body').animate({
                    scrollTop: $("#contact_us").offset().top
                }, 1000);
				 $('#sidebar').removeClass('active');
                    $('.overlay').removeClass('active');

                    
 });
</script>


</body>
</html></div>
<script defer>
   
var company_name, company_rate, company_id, message = jQuery('.contact__msg');
var get_all_architect =true;
jQuery(document).ready(function($) {
	
	
    var table = jQuery('#example').DataTable( {
        responsive: true,
         "order": [],
		pageLength: 50,
        columnDefs: [
            { responsivePriority: 1, targets: 0 },
            { responsivePriority: 2, targets: 1 },
       
        ]
    } );
  

    var post_url = "https://www.naqsha.com.pk/api-call/db/query/registered_companies.php?cat=arch";
//alert(post_url);
  
    var post_url_query=post_url;
	  table.clear().draw();
    

        jQuery.ajax({
            url: post_url_query,
            type: "POST",
            dataType: "json",
             success: function(data) {
              var temp = data;
                console.log(data);
                for (var i = 0; i < data.data.length; i++) {
                    var b =i;
                      table.row.add([
                     
                        '<h5>'+ data.data[i].title +'</h5>',
                        '<a href="https://www.naqsha.com.pk/' + data.data[i].url + '"><button type="button" class="btn btn-warning" >View Portfolio</button></a>'
                    ]).draw(false);

                }
                jQuery(".page_loader").fadeOut("fast");
            },
            error: function(data) {
                console.log(data);
                jQuery(".page_loader").fadeOut("fast");
            }
        });

   
   

});



</script>










