@extends('Admin.admin-layout') @push('css') @endpush @section('content')

<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->

    <!-- BEGIN PAGE CONTENT-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="widget red">
                <div class="widget-title">
                    <h4><i class=" icon-key"></i>Gallery Images</h4>
                    <span class="tools">
               <a href="javascript:;" class="icon-chevron-down"></a>
               <a href="javascript:;" class="icon-remove"></a>
               </span>
                </div>
                <div class="widget-body form">
                    <!-- BEGIN FORM-->
                    @if(isset($gimages) && !empty($gimages))
                    <form class="form-horizontal" method="post" action="{{ route('gimages.update',$gimages->id) }} " enctype="multipart/form-data">
                        @method('put') @else
                        <form class="form-horizontal" method="post" action="{{ route('gimages.store') }}" enctype="multipart/form-data">
                            @endif @csrf
                            
                            <div class="control-group success">
                                <label class="control-label" for="gc_id">Gallery Subcategory </label>
                                <div class="controls">
                                    <select class="span6" name="gsc_id" id="gsc_id" required="">
                                        <option value="">Please Select Gallery SubCategory</option>
                                        @foreach($gsc as $value)
                                        <option value="{{ $value->id }}" @if(isset($gimages) && $value->id = $gimages->gsc_id) selected="selected" @endif>{{ $value->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>


                                <div class="control-group error col-sm-12">
                                    <label class="control-label" for="inputError">Name</label>
                                    <div class="controls">
                                        <input type="text" name="name" id="name" class="form-control" @if(isset($gimages) && !empty($gimages)) value="{{ $gimages->name }}" @endif>
                                    </div>
                                </div>
                        
                                <div class="control-group error col-sm-12">
                                    <label class="control-label" for="inputError">image name</label>
                                    <div class="controls">
                                        <input type="file" name="image_name" id="image_name" class="form-control">
                                    </div>
                                </div>
                               
                               

                                <div class="form-actions">
                                    <button type="submit" class="btn btn-success">
                                        @if(isset($gimages) && !empty($gimages)) Update @else Save @endif
                                    </button>
                                    <button type="button" class="btn">Cancel</button>
                                </div>
                        </form>
                        <!-- END FORM-->
                        </div>
                </div>
                <!-- END VALIDATION STATES-->
            </div>
        </div>

@endsection 
@push('js') 

@endpush


