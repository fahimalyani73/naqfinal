<!DOCTYPE html>
<head>
   <meta charset="utf-8" />
   <title>Naqsha Ghar</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="Mosaddek" name="author" />
   <link href="{{ asset('public/admin-assets')}}/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="{{ asset('public/admin-assets')}}/assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="{{ asset('public/admin-assets')}}/assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="{{ asset('public/admin-assets')}}/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="{{ asset('public/admin-assets')}}/css/style.css" rel="stylesheet" />
   <link href="{{ asset('public/admin-assets')}}/css/style-responsive.css" rel="stylesheet" />
   <link href="{{ asset('public/admin-assets')}}/css/style-default.css" rel="stylesheet" id="style_color" />
   <link href="{{asset('public/admin-assets')}}/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
   <link href="{{ asset('public/admin-assets')}}/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
</head>
