@extends('Admin.admin-layout')

@push('css')
@endpush
@section('content')

<div class="container-fluid">

   <br><br>
   <!-- END PAGE HEADER-->
   <!-- BEGIN PAGE CONTENT-->
   <div class="row-fluid">
      <div class="span12">
         <!-- BEGIN VALIDATION STATES-->
         <div class="widget red">
            <div class="widget-title">
               <h4> Company </h4>
               <span class="tools">
               <a href="javascript:;" class="icon-chevron-down"></a>
               <a href="javascript:;" class="icon-remove"></a>
               </span>
            </div>
            <div class="widget-body form">
               <!-- BEGIN FORM-->
            @isset($company)
               <form  class="form-horizontal" method="post" action="{{ route('company.update',$company->id) }}" enctype="multipart/form-data">
                  @method('put')
            @else
               <form  class="form-horizontal" method="post" action="{{ route('company.store') }}" enctype="multipart/form-data">
            @endisset
                  @csrf
                  <div class="control-group success">
                     <label class="control-label" for="c_name">Name  </label>
                     <div class="controls">
                        <input required="" type="text" class="span6" id="c_name" name="c_name" placeholder="Company Name" @if(isset($company) && !empty($company->c_name)) value="{{ $company->c_name}}" @endif>
                     </div>
                  </div>
                  <div class="control-group error">
                     <label class="control-label" for="c_type">Type</label>
                     <div class="controls">
                        <input required="" type="text" class="span6" id="c_type" name="c_type" placeholder="Company Type" @if(isset($company) && !empty($company->c_type)) value="{{ $company->c_type}}" @endif>
                  
                     </div>
                  </div>
                  <div class="control-group error">
                     <label class="control-label" for="c_drawing_detail">Drawing Detail</label>
                     <div class="controls">
                        <input required="" type="text" class="span6" id="c_drawing_detail" name="c_drawing_detail" placeholder="Company Drawing Detail" @if(isset($company) && !empty($company->c_drawing_detail)) value="{{ $company->c_drawing_detail}}" @endif>
                       
                     </div>
                  </div>
                   <div class="control-group error">
                     <label class="control-label" for="c_profile_image">Company Profile Image</label>
                     <div class="controls">
                        <input type="file" class="span6" id="c_profile_image" name="c_profile_image">
                        
                     </div>
                  </div>
                
                  <div class="form-actions">
                     <button type="submit" class="btn btn-success">
                        @isset($company)
                           Update
                        @else
                           Save
                        @endisset
                     </button>
                     <button type="button" class="btn">Cancel</button>
                  </div>
               </form>
               <!-- END FORM-->
            </div>
         </div>
         <!-- END VALIDATION STATES-->
      </div>
   </div>

</div>
@endsection

@push('js')
@endpush