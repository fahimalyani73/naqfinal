@extends('website.master-layout')

@push('css')

@endpush

@section('content')

<style type="text/css">
@media print { 
 #myModal{
   
 -webkit-print-color-adjust: exact;

	background-image:url(https://www.naqsha.com.pk/wp-content/themes/naqsha/img/bg-email.jpg);
 }
 
}
</style>
<div class=" overview-bgi d-print-none" style="background-image:url('../../assets/images/landscape.jpg');">
   <div class="container">
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12 col-pad">
            <!-- construction Form -->
            
	
	<div class="submit-address dashboard-list">
			
		<div class="row">
				<h3 class="text-center">Landscape</h3>
				<h5 style="padding-left: 30px;
    color: white;">
				Select the options given below 
			</h5>
		</div>
		@include('website.includes.search_filter_form')		
	</div>

         </div>
      </div>
   </div>
   <div class="clearfix visible-xs"></div>
</div>
<div class="row">

            @foreach($cc as $index => $value)

            <div class="col-sm-4 col-md-4 col-lg-3 mt-4">
                <div class="card">
                    <img class="card-img-top" src="https://picsum.photos/200/150/?random
">
                    <div class="card-block">
                        <h4 class="card-title">Landscape</h4>
                        <div class="meta">
                            <a href="#">Town location {{$value->town_location}}</a>
                        </div>
                      
                    
                   
                        <p>
                          <span>
                            Covered area:
                          </span>
                          <span>
                            {{$value->covered_area}}
                          </span>
                        </p>
                        <p>
                          <span>
                            Total
                          </span>
                          <span>
                            {{$value->total}}
                          </span>
                        </p>

                 

                     
                   
                       
                    </div>
                      {{ $value->company->c_name }}
                                    
                                    <a  target="_blank" href="{{ route('company.detail',$value->company_id) }}">
                                        <i class="fa fa-external-link" aria-hidden="true" style="font-size: 12px;"></i>

                                    Profile </a>
                     <div class="card-footer">
                    <div class="row" style="padding-left: 0px;">
                        <div class="col-sm-6" style="padding-left: 2px;">
                  <button class="btn btn-secondary float-left ">Rate/Sqft
                         {{$value->rate_sqft}}</button></div>
                        <div class="col-md-6">
                            
                       
                         <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#myModal" onclick="send_company_id('4474',&quot;Zaheer A Shiekh and Associates&quot;,'0')">Get Quotation</button>
                        </div>
                     </div>
                        
                    </div>
                  </div>
                </div>
                    @endforeach
                </div>
   

@include('website.includes.conpany-detail') 
@include('website.includes.get-quotation')

@include('website.includes.google_recaptcha')

@endsection

@push('script')
  @include('website.includes.company-detail-and-get-quotation-script')
     
@endpush