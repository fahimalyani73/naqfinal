@extends('website.master-layout')

@push('css')

@endpush

@section('content')
	@include('website.includes.header_banners.aboutus-header')
	<div class="overview-bgi listing-banner about-banner-image" >
   <div class="container listing-banner-info">
      <div class="row">
         <div class="col-lg-12 col-md-12 clearfix">
            <div class="text">
               <!--<h1> About Us </h1>-->
            </div>
         </div>
      </div>
   </div>
</div>
<div class="listing-details-page content-area-6 listing-about-page">
   <div class="container">
      <div class="row">
         <div class="col-lg-12 col-md-12">
            <!-- listing description start -->
            <div class="listing-description mb-40" id="about_us_description">
               <p></p>
               <article id="post-4141" class="post-4141 page type-page status-publish has-post-thumbnail hentry">
                  <header class="entry-header">
                  </header>
                  <!-- .entry-header -->
                  <div class="entry-content">
                     <div class="container about-text-justify">
                        <div class="row">
                           <div class="col-lg-12 about-ud-div-text-color">
                              <h2 class="mt-2">About us</h2>
                              <p class="">
                                    <strong>“NQSHA GHAR” </strong> is a company which provides construction industry in Pakistan. It brings professionals and clients together. We sum up all experts on website like Architects, Engineers, Designers and Developers. We deal with all main cities including LHR, KHI, ISD, RWP, SKT, FSD,GWA, MTN and SPR in Pakistan.
                              </p>
                           </div>
                        </div>
                     </div>
                     <style></style>
                     <div class="mt-5">
                        <h2 class="text-center">Connecting Services</h2>
                     </div>
                     <div class="container mt-5 mb-5 text-center">
                        <div class="row">
                           <div class="col-lg-2 box conn-serv-box">
                              <a ><br>
                              <i class="flaticon-blueprint"></i></a>
                              <p></p>
                              <h5 class="color-black">Architecture</h5>
                              <p>&nbsp;</p>
                           </div>
                           <div class="col-lg-2 box conn-serv-box">
                              <a ><br>
                              <i class="flaticon-hook"></i></a>
                              <p></p>
                              <h5 class="">Construction</h5>
                              <p>&nbsp;</p>
                           </div>
                           <div class="col-lg-2 box conn-serv-box">
                              <a ><br>
                              <i class="flaticon-decorating"></i></a>
                              <p></p>
                              <h5 class="">Interior</h5>
                              <p>&nbsp;</p>
                           </div>
                           <div class="col-lg-2 box conn-serv-box">
                              <a ><br>
                              <i class="flaticon-field"></i></a>
                              <p></p>
                              <h5 class="">Landscape</h5>
                              <p>&nbsp;</p>
                           </div>
                           <div class="col-lg-2 box conn-serv-box">
                              <a ><br>
                              <i class="flaticon-cityscape"></i></a>
                              <p></p>
                              <h5 class="">Town Planning</h5>
                              <p>&nbsp;</p>
                           </div>
                           <div class="col-lg-2 box conn-serv-box">
                              <a>
                                 <br>
                              <i class="flaticon-worker"></i></a>
                              <p></p>
                              <h5 class="">Engineering</h5>
                              <p>&nbsp;</p>
                           </div>
                        </div>
                     </div>
                     <p>
                        <img class="alignnone size-full wp-image-6856" src="https://www.naqsha.com.pk/wp-content/uploads/2019/11/about-us.jpg" alt="naqsha" width="1110" height="695" sizes="(max-width: 1110px) 100vw, 1110px">
                     </p>
                     <div class="container">
                        <div class="row">
                           <div class="col-lg-12"></div>
                        </div>
                     </div>
                     <div class="container-fluid mt-4 vision-statement-main-div">
                        <div class="row">
                           <div class="col-lg-6 ml-3 mr-2 mt-2 vision-statement-sub-div">
                              <h2 class="mt-4 text-center">Vision Statement</h2>
                              <p class="mt-4 mr-4 ml-4">
                                 <strong>“Naqsha” </strong>brings an innovation to the construction sector of Pakistan by connecting public with building professionals. We are striving hard to achieve success and excellence for both parties (clients &amp; professionals).
                              </p>
                           </div>
                           <div class="col-lg-5 ml-3 mr-2 mt-2 vision-statement-sub-div">
                              <h2 class="mt-4 text-center">Mission Statement</h2>
                              <p class="mt-4 mr-4 ml-4">Our goal is to elevate the business of building professionals an online marketing platform.</p>
                           </div>
                           <div class="col-lg-12 mt-5">
                              <h4 class="text-center">CURRENT SITUATION OF MARKET</h4>
                              <p class="mr-5 ml-5 mt-2">
                                 It is very hard for the public to select appropriate professional for their project design/execution. Conversely, it is also challenging for a professional to locate a refined client.
                              </p>
                           </div>
                           <div class="col-lg-12 mt-4">
                              <h4 class="text-center">
                                 SOLUTION TO THE PROBLEM
                              </h4>
                              <p class="mr-5 ml-5 mt-2 text-center">
                                 <strong>“Naqsha” </strong>offers public to get quotation according to their budget. We are here to provide the solution by connecting clients with the required building professionals in the following services:
                              </p>
                           </div>
                        </div>
                     </div>
                     <div class="mt-4 mt-cus">
                        <h2 class="text-center">
                           Naqsha Services
                        </h2>
                     </div>
                     <div class="container mt-5 mt-cus-2">
                        <div class="row">
                           <div class="offset-1 col-lg-5 p-5 mrg-top">
                              <div></div>
                              <h3 class="mt-3 mb-3">For Professionals</h3>
                              <ol>
                                 <li class="mb-1">Boost Your Business Online</li>
                                 <li class="mb-1">24/7 anywhere everywhere</li>
                                 <li class="mb-1">Public and approach

you through</li>
                                 <li class="mb-1">Company location</li>
                                 <li class="mb-1">Estimated location</li>
                                 <li class="mb-1">
                                    Previous projects
                                    <ul>
                                       <li class="mb-1 color-black">a) Contact details</li>
                                       <li class="mb-1 color-black" >b) Earning Profits</li>
                                       <li class="mb-1 color-black" >c) Online Directory</li>
                                       <li>d) Wide Range

Contacts</li>
                                    </ul>
                                 </li>
                              </ol>
                           </div>
                           <div class="col-lg-6 pl-0 pr-0"><img class="size-full alignright" src="http://www1.naqsha.com.pk/wp-content/uploads/2019/10/for-professional.png" alt="" width="100%" height="400"></div>
                        </div>
                        <div class="row mt-cus-3">
                           <div class="col-lg-6 pl-0 pr-0"><img class="size-full alignleft" src="http://www1.naqsha.com.pk/wp-content/uploads/2019/10/banneerr.png" alt="" width="100%" height="400"></div>
                           <div class="offset-1 col-lg-5 p-5 mrg-top">
                              <h3 class="mt-3 mb-3">For Public</h3>
                              
                              <ol>
                                 <li class="mb-1">Area/ Town/ City</li>
                                 <li class="mb-1">Company Rates</li>
                                 <li class="mb-1">Online Record</li>
                                 <li class="mb-1">Estimated Quotation</li>
                                 <li class="mb-1">Contact Details</li>
                                 <li class="mb-1">Customer Services</li>
                              </ol>
                           </div>
                        </div>
                     </div>
                     <div class="services content-area about-service-div">
                        <div class="container">
                           <p>
                              <!-- Main title -->
                           </p>
                           <div class="main-title text-center about-main-title-margin">
                              <h1 >How can you get professionals from our website</h1>
                              <p>Get a quotation in three simple steps.</p>
                           </div>
                           <div class="row">
                              <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
                                 <div class="service-info">
                                    <div class="icon"></div>
                                    <h3>Simple Search</h3>
                                    <p>Search using appropriate filters.</p>
                                 </div>
                              </div>
                              <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
                                 <div class="service-info">
                                    <div class="icon"></div>
                                    <h3>Check Portfolios and Pricing</h3>
                                    <p>View company portfolios and select your builder/designer.</p>
                                 </div>
                              </div>
                              <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
                                 <div class="service-info">
                                    <div class="icon"></div>
                                    <h3>Get Quotation/ Contact Professional</h3>
                                    <p>Select your best quotation from result.</p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- .entry-content -->
               </article>
               <!-- #post-## -->
               <p></p>
            </div>
            <div class="col-lg-4 col-md-4">
            </div>
         </div>
      </div>
   </div>
</div>
@endsection

@push('script')

@endpush