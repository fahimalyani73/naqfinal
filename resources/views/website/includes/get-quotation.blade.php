<!-- The Modal -->
<div class="modal d-print-block" id="myModal">
    <div class="modal-dialog modal-dialog-scrollable modal-lg">
        <div class="modal-content">
            <div class="d-none d-print-block text-center p-3"> 
                <img height="80px" src="{{ asset('public/assets/images/naqshalogo.jpeg') }}">
            </div>
            <!-- Modal Header -->
            <div class="modal-header">
                <h5 class="modal-title" style="margin-left: 30% ! important; ">Estimated Quotation</h5>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <!-- Modal body -->
            <div class="widget booking-now  d-xl-block d-lg-block" style="overflow-y: auto; margin-bottom: 0 !important;">

                <form method="post" class="mb-30" id="get_quote" action="{{ route('sendEmail') }}">
                    @csrf
                    <input type="hidden" name="company_id" id="company_id">
                    <input type="hidden" name="company_name" id="company_name">
                    <input type="hidden" name="company_rate" id="company_rate">
                    <div class="row">
                        <div class="form-group col-12 col-sm-12 col-md-12">
                            <h6>Name:</h6>
                            <input type="text" name="full_name" id="full_name" class="form-control" placeholder="Write your full name">
                        </div>

                        <div class="form-group col-12 col-sm-12 col-md-12">
                            <h6>Email:</h6>
                            <input type="email" name="email" id="email" class="form-control" placeholder="Write your e-mail">
                        </div>
                        <div class="form-group col-12 col-sm-12 col-md-12">
                            <h6>Mobile:</h6>
                            <input type="text" name="mobile" id="mobile" class="form-control" placeholder="Write your mobile number">
                        </div>
                    </div>
                    <div class="form-group">
                        <style type="text/css">
                            .g-recaptcha:first-child {
                                margin: 0px auto !important;
                            }
                        </style>
                        <div class="g-recaptcha" data-callback="recaptchaCallback" data-sitekey="6LfglbAUAAAAAEbvJytqXNUf5yUTklAjX89yNThm"></div>
                    </div>
                    <div class="row">
                        <div class="col-12">

                            <div class="contact__msg alert alert-danger" style="display:none !important;">
                                <p>Your message was sent successfully.</p>
                            </div>
                        </div>
                    </div>

                    <table class="table table-hover table-striped" id='table_quote'>

                    </table>

                    <div class="form-group mb-0 row">

                        <button class="col-12 col-sm-12 col-md-8 ml-4 btn btn-lg  btn-warning d-print-none" type="submit" id="submit_quote" style="font-weight: bold;font-size: 12px;margin-top: 10px;" >Schedule your meeting with professionals </button>
                        <!-- <button class="col-12 col-sm-12 col-md-3 btn btn-lg  btn-warning d-print-none" type="button" id="print_quote" style="margin-left: 5px;font-size: 13px;margin-top: 10px;font-weight: bold;"><i class="lnr lnr-printer"></i> Print Quote </button> -->

                    </div>

                </form>
            </div>

            <!-- Modal footer -->

        </div>
    </div>
</div>