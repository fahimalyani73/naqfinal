@push('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<style type="text/css">
   #btnSearch{
      margin-top: 29px;
  }
  .select2-container--default .select2-selection--single{
      background: transparent !important;
  }
</style>

@endpush


<div class="contact-2">
   <div class="row pad-2">
<form  method="get" 
   @if(Request::is('architecture') || Request::is('architecture/*'))
      action="{{ route('home.architecture') }}" 
   @elseif(Request::is('construction') || Request::is('construction/*'))
      action="{{ route('home.construction') }}" 
   @elseif(Request::is('interior') || Request::is('interior/*'))
      action="{{ route('home.interior') }}" 
   @elseif(Request::is('landscape') || Request::is('landscape/*'))
      action="{{ route('home.landscape') }}" 
   @elseif(Request::is('town-planner') || Request::is('town-planner/*'))
      action="{{ route('home.town.planner') }}" 
   @endif
   
>

   <div class="col-sm-12">
   <div class="row">
       
    @if(!empty($data) && array_key_exists('pro_location',$data[0]) !== false)
      <div class="col-md-4 col-sm-12">
         <div class="form-group css_custom_select">
            <label>Professional Location</label>
            <select class="form-control select2" name="professional_location" id="professional_location">
               <option value="">Select Professional Location</option>
        
               @if(isset($data) && !empty($data))
               @php 
                    $pro_location = array_column($data, 'pro_location');
                    $pro_location = array_unique($pro_location);
                @endphp
                
               @foreach($pro_location as $key => $value)
                
                <option value="{{ $value }}">
                   {{ $value }}
                </option>
              
               @endforeach
               @endif
            </select>
       
         </div>
      </div>
     @endif
     @if(!empty($data) && array_key_exists('plot_location',$data[0]) !== false)
      <div class="col-md-4 col-sm-12">
         <div class="form-group css_custom_select">
            <label>Plot Location </label>
            <select class="form-control select2" name="plot_location" id="plot_location">
               <option value="">Select Plot Location</option>
               @if(isset($data) && !empty($data))
               @php 
                    $plot_location = array_column($data, 'plot_location');
                    $plot_location = array_unique($plot_location);
                @endphp
                
               @foreach($plot_location as $key => $value)

               <option value="{{ $value }}">{{ $value }}</option>
            
               @endforeach
               @endif
            </select>
   
         </div>
      </div>
    @endif
    @if(!empty($data) && array_key_exists('plot_type',$data[0]) !== false)
      <div class="col-md-4 col-sm-12">
         <div class="form-group">
            <label>Type</label>
            <select class="form-control select2" id="plot_type" name="plot_type">
               <option value="">Select Type</option>
             
               @if(isset($data) && !empty($data))
               @php 
                    $plot_type = array_column($data, 'plot_type');
                    $plot_type = array_unique($plot_type);
                @endphp
               @foreach($plot_type as $key => $value)
               
               <option value="{{ $value }}">{{ $value }}</option>
              
               @endforeach
               @endif
            </select>
         </div>
      </div>
    @endif
        
        @if(!empty($data) && array_key_exists('plot_size',$data[0]) !== false)
          <div class="col-md-4 col-sm-12">
             <div class="form-group">
                <label>Plot Size</label>
                <select class="form-control select2" id="plot_size" name="plot_size">
                   <option value="">Select Plot Size</option>
                  
                   @if(isset($data) && !empty($data))
                   @php 
                        $plot_size = array_column($data, 'plot_size');
                        $plot_size = array_unique($plot_size);
                    @endphp
                   @foreach($plot_size as $key => $value)
                  
                   <option value="{{ $value }}">{{ $value }}</option>
                   
                   @endforeach
                   @endif
               
                </select>
             </div>
          </div>
        @endif

        
        @if(!empty($data) && array_key_exists('covered_area',$data[0]) !== false)
          <div class="col-md-4 col-sm-12">
             <div class="form-group">
                <label>Covered Area</label>
                <input class="form-control " id="covered_area" name="covered_area" value="1000">
           
             </div>
          </div>
        @endif
        @if(!empty($data) && array_key_exists('package',$data[0]) !== false)
          <div class="col-md-4 col-sm-12">
             <div class="form-group">
                <label>Package</label>
                <select class="form-control select2" id="package" name="package">
                   <option value="">Select Plot Size</option>
                   
                   @if(isset($data) && !empty($data))
                   @php 
                        $package = array_column($data, 'package');
                        $package = array_unique($package);
                    @endphp
                   @foreach($package as $key => $value)
                    
                   <option value="{{ $value }}">{{ $value }}</option>
                  
                   @endforeach
                   @endif
               
                </select>
             </div>
          </div>
        @endif
   
    @if(!empty($data) && array_key_exists('no_floor',$data[0]) !== false)
      <div class="col-md-4 col-sm-12">
         <div class="form-group" id="floor_main_div">
            <label>No. of Floors</label>
            <select class="form-control select2" name="no_floor" id="no_floor">
               <option value="">Select no of floor</option>
              
               @if(isset($data) && !empty($data))
               @php 
                    $no_floor = array_column($data, 'no_floor');
                    $no_floor = array_unique($no_floor);
                @endphp
               @foreach($no_floor as $key => $value)
               
               <option value="{{ $value }}">{{ $value }}</option>
              
               @endforeach
               @endif
            </select>
         </div>
      </div>
    @endif
      <div class="col-md-4 col-sm-12">
         <div class="form-group mb-0 search-button-mt">
            <button type="submit" id="btnSearch" class="btn btn-primary" type="button" style="width:100%;">Search</button>
         </div>
      </div>
   </div>

         </form>
      </div>
   </div>
</div>
@push('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
@endpush
