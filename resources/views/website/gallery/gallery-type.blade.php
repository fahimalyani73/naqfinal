@extends('website.master-layout')

@push('css')

@endpush
@php 
    $banner_image = isset($gsc[0]['gallleryCategory']['gc_image']) ? $gsc[0]['gallleryCategory']['gc_image'] : '';
@endphp
@section('content')
	@include('website.includes.header_banners.aboutus-header')
	<div class="overview-bgi listing-banner" style="background-image:url('../images/settings/{{ $banner_image }}'); background-repeat: repeat; background-size: cover;">
    <div class="container listing-banner-info">
        <div class="row">
            <div class="col-lg-12 col-md-12 clearfix">
                <div class="text">
                    <h1> 
                        {{ isset($gsc[0]['gallleryCategory']['gc_name']) ? $gsc[0]['gallleryCategory']['gc_name'] : 'Gallary Type' }}
                    </h1>
                   
                </div>
            </div>
            <div class="col-lg-5 col-md-12 clearfix">
                <div class="cover-buttons">
                    <ul>
                      
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="listing-section content-area d-print-none" style="background-image:url('../images/settings/{{ $banner_image }}') none;">
	<div class="container">
		<div class="row">
			<div class="col">
               <div class="row">
                    
              @if(isset($gsc) && !empty($gsc))
              @foreach($gsc as $key => $value)
				<div class="blog-1 col-4 aos-init" data-aos="flip-left">
                    <div class="blog-photo">
                     <img src="{{ asset('images/settings'.'/'.$value->image_name) }}" alt="small-blog" class="img-fluid" style="width: 350px; height: 195px;">       
                    </div>
                    <div class="detail">
                        <h3>
                            <a href="{{ route('kanal.2.residential',$value->id) }}"> {{ $value->gallleryCategory['gc_name'] }} </a>
                        </h3>
                       
                        <a href="{{ route('kanal.2.residential',$value->id) }}" class="read-more">View more</a>
                    </div>
                </div>
            @endforeach
            @endif


                {{-- <div class="blog-1 col-4 aos-init" data-aos="flip-left">
                    <div class="blog-photo">
                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/12/afaq-builders-naqsha-karachi-pakistan-2.jpg" alt="small-blog" class="img-fluid">
                    </div>
                    <div class="detail">
                        <h3>
                            <a href="{{ route('d3.evlevation') }}">3D Elevation</a>
                        </h3>
                       
                        <a href="{{ route('d3.evlevation') }}" class="read-more">View more</a>
                    </div>
                </div>
                <div class="blog-1 col-4 aos-init" data-aos="flip-left">
                    <div class="blog-photo">
                     <img src="https://www.naqsha.com.pk/wp-content/uploads/2020/01/1-kanal-house.jpg" alt="small-blog" class="img-fluid">

                    </div>
                    <div class="detail">
                        <h3>
                            <a href="{{ route('one.kanal.house') }}">Residential</a>
                        </h3>
                       
                        <a href="{{ route('one.kanal.house') }}" class="read-more">View more</a>
                    </div>
                </div> --}}
            </div>

		   </div>
		</div>
	</div>
</div>
@endsection

@push('script')

@endpush