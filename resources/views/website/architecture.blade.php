@extends('website.master-layout') 
@push('css') 

<style type="text/css">
    html {
    font-family: Lato, 'Helvetica Neue', Arial, Helvetica, sans-serif;
    font-size: 14px;
}

h5 {
    font-size: 1.28571429em;
    font-weight: 700;
    line-height: 1.2857em;
    margin: 0;
}

.card {
    font-size: 1em;
    overflow: hidden;
    padding: 0;
    border: none;
    border-radius: .28571429rem;
    box-shadow: 0 1px 3px 0 #d4d4d5, 0 0 0 1px #d4d4d5;
}

.card-block {
    font-size: 1em;
    position: relative;
    margin: 0;
    padding: 1em;
    border: none;
    border-top: 1px solid rgba(34, 36, 38, .1);
    box-shadow: none;
}

.card-img-top {
    display: block;
    width: 100%;
    height: auto;
}

.card-title {
    font-size: 1.28571429em;
    font-weight: 700;
    line-height: 1.2857em;
}

.card-text {
    clear: both;
    margin-top: .5em;
    color: rgba(0, 0, 0, .68);
}

.card-footer {
    font-size: 1em;
    position: static;
    top: 0;
    left: 0;
    max-width: 100%;
    padding: .75em 1em;
    color: rgba(0, 0, 0, .4);
    border-top: 1px solid rgba(0, 0, 0, .05) !important;
    background: #fff;
}

.card-inverse .btn {
    border: 1px solid rgba(0, 0, 0, .05);
}

.profile {
    position: absolute;
    top: -12px;
    display: inline-block;
    overflow: hidden;
    box-sizing: border-box;
    width: 25px;
    height: 25px;
    margin: 0;
    border: 1px solid #fff;
    border-radius: 50%;
}

.profile-avatar {
    display: block;
    width: 100%;
    height: 100%;
    border-radius: 50%;
}

.profile-inline {
    position: relative;
    top: 0;
    display: inline-block;
}

.profile-inline ~ .card-title {
    display: inline-block;
    margin-left: 4px;
    vertical-align: top;
}

.text-bold {
    font-weight: 700;
}

.meta {
    font-size: 1em;
    color: rgba(0, 0, 0, .4);
}

.meta a {
    text-decoration: none;
    color: rgba(0, 0, 0, .4);
}

.meta a:hover {
    color: rgba(0, 0, 0, .87);
}
</style>
@section('content')

<div class=" overview-bgi d-print-none" style="background-image:url(' ../../assets/images/architecture-design.jpg');">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-pad">
                <!-- construction Form -->
                <div class="submit-address dashboard-list">
                    
                    <div class="row">
                         <h3 class="text-center">Architecture</h3>
                         <h5 style="padding-left: 30px;
                            color: white;">
                            Select the options given below 
                         </h5>
                      </div>
                      @include('website.includes.search_filter_form')

                </div>

            </div>
        </div>
    </div>
    <div class="clearfix visible-xs"></div>
</div>
<div class="container">
        <div class="row">

              @foreach($cc as $index => $value)
                    
            <div class="col-sm-4 col-md-4 col-lg-3 mt-4">

                <div class="card">
                    <img class="card-img-top" src="https://picsum.photos/200/150/?random
">
                    <div class="card-block">
                        <h4 class="card-title">Architecture</h4>
                        Plot location {{$value->plot_location}}
                        <div class="meta">
                            <a href="#"></a>
                        </div>

                    <!--     <div class="card-text">
                            Detail {{$value->details}}
                            <input type="text" name="details">
                        </div> -->
                     <p>
                         <span>
                             Type:
                         </span>
                         <span>
                             {{$value->plot_type}}
                         </span>
                     </p>
                        
                       
                    </div>

                     {{ $value->company->c_name }}
                                    
                                    <a  target="_blank" href="{{ route('company.detail',$value->company_id) }}">
                                        <i class="fa fa-external-link" aria-hidden="true" style="font-size: 12px;"></i>

                                    Profile </a>
                    <div class="card-footer">
                    <div class="row" style="padding-left: 0px;">
                        <div class="col-sm-6" style="padding-left: 2px;">
                  <button class="btn btn-secondary float-left ">Rate/Sqft
                          {{$value->rate}}</button></div>
                        <div class="col-md-6">
                            
                       
                            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#myModal" onclick="send_company_id('4474',&quot;Zaheer A Shiekh and Associates&quot;,'0')">Get Quotation</button>
                        </div>
                     </div>
                        
                    </div>
                </div>
            </div>
       
            
   @endforeach
        </div>

</div>
@endsection
@push('script')
@include('website.includes.company-detail-and-get-quotation-script')
@endpush


 