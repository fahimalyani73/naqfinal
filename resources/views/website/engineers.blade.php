@extends('website.master-layout')

@push('css')

@endpush

@section('content')
<div class=" overview-bgi d-print-none" style="background-image:url(https://www.naqsha.com.pk/wp-content/themes/naqsha/img/cover/cons.jpg );">
   <div class="container">
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12 col-pad">
            <!-- construction Form -->
            <form method="post" id="landscape_form">
               <div class="submit-address dashboard-list">
                  <div class="row">
                     <h3 class="text-center">Engineering</h3>
                  </div>
                  @include('website.includes.search_filter_form')
               </div>
            </form>
         </div>
      </div>
   </div>
   <div class="clearfix visible-xs"></div>
</div>

@include('website.includes.image_popup_models')
@endsection

@push('script')

@endpush