@extends('website.master-layout')

@push('css')

@endpush

@section('content')

<div class="overview-bgi listing-banner" @isset($cp) style="background-image:url('{{ "../public/images/cp_slider_images/$cp->cp_slider_image" }}');background-size: 100% 100%;" @endisset>
    <div class="container listing-banner-info">
        <div class="row">
            <div class="col-lg-7 col-md-12 clearfix">
                <div class="text">
                    <h1> @isset($data['cpcd']) {{ $data['cpcd']->c_fullname }} @endisset </h1>
                    
                        
                </div>
            </div>
            <div class="col-lg-5 col-md-12 clearfix">
                <div class="cover-buttons">
                    <ul>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="listing-details-page content-area-6" style="padding-top:40px">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-12">
                <!-- listing description start -->
                <div class="listing-description mb-40 text-justify">
                    <h3 class="heading-2">
            About Us
            </h3>
                    <p>
                    </p>
                    <p>
                       @isset($cp){{ $cp->cp_about_us }}@endisset
                      <!--   Zaheer A Shiekh and Associates, we are passionate about making your home building dreams a reality. By honoring the personal vision of each customer while providing high-quality, complete construction services, we can transform your dreams into a beautiful, unique home. -->
                    </p>
                    <p>&nbsp;</p>
                    <p></p>
                </div>

                <!-- gallery start -->
                <div class="gallery">
                    <h3 class="heading-2"> Gallery </h3>
                    <div class="container">
                        <div class="slick-slider-area">
                            <div class="row slick-carousel slick-initialized slick-slider" data-slick="{&quot;slidesToShow&quot;: 1, &quot;responsive&quot;:[{&quot;settings&quot;:{&quot;slidesToShow&quot;: 1}}, {&quot;settings&quot;:{&quot;slidesToShow&quot;: 1}}]}">
                                <div class="slick-list draggable" style="padding: 0px;">
                                    <div class="slick-track" style="opacity: 1; ">
                                        @isset($data['cpg'])
                                        @foreach($data['cpg'] as $index => $value )
                                        <div class="slick-slide slick-cloned" data-slick-index="-2" aria-hidden="true" tabindex="-1" style="width: 730px;">
                                            <div>
                                                <div class="slick-slide-item" style="width: 100%; display: inline-block;">
                                                    <div class="portfolio-item">
                                                        <img src="{{ asset('public/images/cp_gallery_images/'.$value->cp_gallery_image) }}" class="img-fluid">
                                                        <div class="portfolio-content">
                                                            <div class="portfolio-content-inner">
                                                                <p>
                                                                   {{ $value->cp_gallery_image_name }} </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                        @endisset
                                       <!--  <div class="slick-slide slick-cloned" data-slick-index="-1" aria-hidden="true" tabindex="-1" style="width: 730px;">
                                            <div>
                                                <div class="slick-slide-item" style="width: 100%; display: inline-block;">
                                                    <div class="portfolio-item">
                                                        <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/07/ZASA-presentaion-140.jpg" alt="ZASA presentaion-140" class="img-fluid">
                                                        <div class="portfolio-content">
                                                            <div class="portfolio-content-inner">
                                                                <p>
                                                                    ZASA presentaion-140 </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->
                                      <!--   <div class="slick-slide" data-slick-index="0" aria-hidden="true" tabindex="-1" style="width: 730px;">
                                            <div>
                                                <div class="slick-slide-item" style="width: 100%; display: inline-block;">
                                                    <div class="portfolio-item">
                                                        <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/07/ZASA-presentaion-027.jpg" alt="ZASA presentaion-027" class="img-fluid">
                                                        <div class="portfolio-content">
                                                            <div class="portfolio-content-inner">
                                                                <p>
                                                                    ZASA presentaion-027 </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slick-slide slick-current slick-active slick-center" data-slick-index="1" aria-hidden="false" style="width: 730px;">
                                            <div>
                                                <div class="slick-slide-item" style="width: 100%; display: inline-block;">
                                                    <div class="portfolio-item">
                                                        <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/07/ZASA-presentaion-072.jpg" alt="ZASA presentaion-072" class="img-fluid">
                                                        <div class="portfolio-content">
                                                            <div class="portfolio-content-inner">
                                                                <p>
                                                                    ZASA presentaion-072 </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slick-slide" data-slick-index="2" aria-hidden="true" tabindex="-1" style="width: 730px;">
                                            <div>
                                                <div class="slick-slide-item" style="width: 100%; display: inline-block;">
                                                    <div class="portfolio-item">
                                                        <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/07/ZASA-presentaion-091.jpg" alt="ZASA presentaion-091" class="img-fluid">
                                                        <div class="portfolio-content">
                                                            <div class="portfolio-content-inner">
                                                                <p>
                                                                    ZASA presentaion-091 </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slick-slide" data-slick-index="3" aria-hidden="true" tabindex="-1" style="width: 730px;">
                                            <div>
                                                <div class="slick-slide-item" style="width: 100%; display: inline-block;">
                                                    <div class="portfolio-item">
                                                        <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/07/ZASA-presentaion-097.jpg" alt="ZASA presentaion-097" class="img-fluid">
                                                        <div class="portfolio-content">
                                                            <div class="portfolio-content-inner">
                                                                <p>
                                                                    ZASA presentaion-097 </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slick-slide" data-slick-index="4" aria-hidden="true" tabindex="-1" style="width: 730px;">
                                            <div>
                                                <div class="slick-slide-item" style="width: 100%; display: inline-block;">
                                                    <div class="portfolio-item">
                                                        <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/07/ZASA-presentaion-137.jpg" alt="ZASA presentaion-137" class="img-fluid">
                                                        <div class="portfolio-content">
                                                            <div class="portfolio-content-inner">
                                                                <p>
                                                                    ZASA presentaion-137 </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slick-slide" data-slick-index="5" aria-hidden="true" tabindex="-1" style="width: 730px;">
                                            <div>
                                                <div class="slick-slide-item" style="width: 100%; display: inline-block;">
                                                    <div class="portfolio-item">
                                                        <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/07/ZASA-presentaion-140.jpg" alt="ZASA presentaion-140" class="img-fluid">
                                                        <div class="portfolio-content">
                                                            <div class="portfolio-content-inner">
                                                                <p>
                                                                    ZASA presentaion-140 </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slick-slide slick-cloned" data-slick-index="6" aria-hidden="true" tabindex="-1" style="width: 730px;">
                                            <div>
                                                <div class="slick-slide-item" style="width: 100%; display: inline-block;">
                                                    <div class="portfolio-item">
                                                        <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/07/ZASA-presentaion-027.jpg" alt="ZASA presentaion-027" class="img-fluid">
                                                        <div class="portfolio-content">
                                                            <div class="portfolio-content-inner">
                                                                <p>
                                                                    ZASA presentaion-027 </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slick-slide slick-cloned" data-slick-index="7" aria-hidden="true" tabindex="-1" style="width: 730px;">
                                            <div>
                                                <div class="slick-slide-item" style="width: 100%; display: inline-block;">
                                                    <div class="portfolio-item">
                                                        <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/07/ZASA-presentaion-072.jpg" alt="ZASA presentaion-072" class="img-fluid">
                                                        <div class="portfolio-content">
                                                            <div class="portfolio-content-inner">
                                                                <p>
                                                                    ZASA presentaion-072 </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slick-slide slick-cloned" data-slick-index="8" aria-hidden="true" tabindex="-1" style="width: 730px;">
                                            <div>
                                                <div class="slick-slide-item" style="width: 100%; display: inline-block;">
                                                    <div class="portfolio-item">
                                                        <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/07/ZASA-presentaion-091.jpg" alt="ZASA presentaion-091" class="img-fluid">
                                                        <div class="portfolio-content">
                                                            <div class="portfolio-content-inner">
                                                                <p>
                                                                    ZASA presentaion-091 </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slick-slide slick-cloned" data-slick-index="9" aria-hidden="true" tabindex="-1" style="width: 730px;">
                                            <div>
                                                <div class="slick-slide-item" style="width: 100%; display: inline-block;">
                                                    <div class="portfolio-item">
                                                        <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/07/ZASA-presentaion-097.jpg" alt="ZASA presentaion-097" class="img-fluid">
                                                        <div class="portfolio-content">
                                                            <div class="portfolio-content-inner">
                                                                <p>
                                                                    ZASA presentaion-097 </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slick-slide slick-cloned" data-slick-index="10" aria-hidden="true" tabindex="-1" style="width: 730px;">
                                            <div>
                                                <div class="slick-slide-item" style="width: 100%; display: inline-block;">
                                                    <div class="portfolio-item">
                                                        <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/07/ZASA-presentaion-137.jpg" alt="ZASA presentaion-137" class="img-fluid">
                                                        <div class="portfolio-content">
                                                            <div class="portfolio-content-inner">
                                                                <p>
                                                                    ZASA presentaion-137 </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slick-slide slick-cloned" data-slick-index="11" aria-hidden="true" tabindex="-1" style="width: 730px;">
                                            <div>
                                                <div class="slick-slide-item" style="width: 100%; display: inline-block;">
                                                    <div class="portfolio-item">
                                                        <img src="https://www.naqsha.com.pk/wp-content/uploads/2019/07/ZASA-presentaion-140.jpg" alt="ZASA presentaion-140" class="img-fluid">
                                                        <div class="portfolio-content">
                                                            <div class="portfolio-content-inner">
                                                                <p>
                                                                    ZASA presentaion-140 </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                            <div class="slick-prev slick-arrow-buton">
                                <i class="fa fa-angle-left"></i>
                            </div>
                            <div class="slick-next slick-arrow-buton">
                                <i class="fa fa-angle-right"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Location start -->

            </div>
            <!-- Services -->
            <div class="col-lg-4 col-md-4">

                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th colspan="2" class="text-center" style="color:black !important;">Contact Details</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row"><i class="fa fa-cogs" aria-hidden="true"></i></th>
                            <td>
                               @isset($data['cpcd']) {{ $data['cpcd']['c_fullname'] }} @endisset
                            </td>

                        </tr>
                        <tr>
                            <th scope="row"><i class="fa fa-map-marker"></i></th>
                            <td>
                                @isset($data['cpcd']) {{ $data['cpcd']['c_address'] }} @endisset
                            </td>
                        </tr>
                        <tr>
                            <th scope="row"><i class="fa fa-user" aria-hidden="true"></i></th>
                            <td>
                               @isset($data['cpcd']) {{ $data['cpcd']['c_owner_name'] }} @endisset
                            </td>
                        </tr>
                        <tr>
                            <th scope="row"><i class="fa fa-phone "></i></th>
                            <td>
                                @isset($data['cpcd'])  {{ $data['cpcd']->c_mobile_number }} @endisset
                                <a href="https://wa.me/+92042-35711684-5?text=I'm%20interested%20in%20your%20rates"><i class="fa fa-whatsapp p-2" style="width: 30px !important;text-align: center;width: 30px !important;color:white;background-color: green"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row"><i class="fa fa-envelope-o"></i></th>
                            <td>
                               @isset($data['cpcd']) {{ $data['cpcd']->c_email }} @endisset
                                
                            </td>

                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@push('script')

@endpush